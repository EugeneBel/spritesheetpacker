# SpriteSheetPacker

### Установка
В ~/.bashrc добавить строку
```
alias sprite_build='python3 /PATH/sprite_sheet_packer.py'
```
Затем
```
source ~/.bashrc
```
### Использование
Из папки с фреймами вызовите ```sprite_build``` или передайте путь к папке с файлами.

В папке с фреймами появится тайтл.

Фреймы должны быть именованны следующим образом: frame0000.png ... frameNNNN.png (стандартный вывод krita).
