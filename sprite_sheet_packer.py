import sys
import os
import re
import math
from PIL import Image


def main():
    print('Sprite Sheet Packer')

    path = os.getcwd()
    if len(sys.argv) > 1:
        path = sys.argv[1]
    files = os.listdir(path)
    mask_file_re = re.compile('^frame[0-9]{4}\.png$')
    number_re = re.compile('[0-9]{4}')

    files_to_merge = []
    for file in files:
        if mask_file_re.findall(file):
            number = int(number_re.search(file)[0])
            files_to_merge.append((number, file))
    files_to_merge = sorted(files_to_merge, key=lambda x: x[0])
    print(f'Detected frame fiels {len(files_to_merge)}')

    if not files_to_merge:
        return

    x_min = 999999
    y_min = 999999
    x_max = 0
    y_max = 0

    for file in files_to_merge:
        image_path_file = os.path.join(path, file[1])
        image = Image.open(image_path_file)

        size = image.size
        for y in range(0, size[1]):
            for x in range(0, size[0]):
                if image.getpixel((x, y))[3] > 0:
                    if x < x_min:
                        x_min = x
                    if y < y_min:
                        y_min = y
                    if x > x_max:
                        x_max = x
                    if y > y_max:
                        y_max = y

    quantity_files = len(files_to_merge)
    delta = math.sqrt(quantity_files)
    max_row = int(delta)
    max_column = math.ceil(quantity_files / max_row)

    x_max += 1
    y_max += 1

    print(f'Detected crop {x_min} {y_min} {x_max} {y_max}')

    d_x = x_max - x_min
    d_y = y_max - y_min

    if d_x < d_y:
        max_row, max_column = max_column, max_row

    print(f'Size sprite sheet {max_row} {max_column}')

    sprite_sheet_size = (d_x * max_row, d_y * max_column)

    image_sprite_sheet = Image.new('RGBA', sprite_sheet_size)

    number = -1
    for column in range(0, max_column):
        for row in range(0, max_row):
            number += 1
            if number < len(files_to_merge):
                image_path_file = os.path.join(path, files_to_merge[number][1])
                image = Image.open(image_path_file)

                for y in range(y_min, y_max):
                    for x in range(x_min, x_max):
                        y_sprite = (y - y_min) + d_y * column
                        x_sprite = (x - x_min) + d_x * row
                        image_sprite_sheet.putpixel((x_sprite, y_sprite), image.getpixel((x, y)))

    print(f'Sprite size is {d_x} {d_y}')

    image_sprite_sheet.save(f'sprite_sheet_{d_x}_{d_y}.png')
    print('Completed')


if __name__ == '__main__':
    main()
